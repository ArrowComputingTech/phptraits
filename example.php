<?php

  trait DatabaseHelper {
    function getRecords() {
      echo "Here are your records";
    }
  }

  class Student {
    use DatabaseHelper;
    function connectDB() {
      $this->getRecords();
    }
  }

  $stu1 = new Student();
  $stu1->connectDB();

?>
