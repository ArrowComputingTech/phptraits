<?php

  trait DatabaseHelper {
    function getRecords() {
      echo "Here are your records.<br>";
    }
  }

  trait StringHelper {
    function removeSpaces($str) {
      return trim($str);
    }
  }

  class Student {
    use DatabaseHelper;
    use StringHelper;
    function connectDB() {
      $this->getRecords();
      echo $this->removeSpaces("   This is some test text. It should not have any leading or trailing spaces.   <br>");
    }
  }

  $stu1 = new Student();
  $stu1->connectDB();

?>
